package session

import (
	"gitee.com/hongliu9527/common/infra/common"
	"gitee.com/hongliu9527/common/infra/session_manager/config"
	"gitee.com/hongliu9527/common/infra/session_manager/memory"

	"gitee.com/hongliu9527/go-tools/logger"
)

// manager 单例会话管理器基础设施对象
var manager common.SessionManagerInfra

// New 创建会话管理器基础设施
func New(managerConfig *config.SessionManagerInfraConfig) common.SessionManagerInfra {
	switch managerConfig.StoreType {
	case config.MemorySession: // 如果是内存会话管理器
		manager = memory.New(managerConfig)
	case config.RedisSession:
		// TODO: 实现基于Redis的会话管理器
	default:
		logger.Error("创建会话管理器基础设施失败(不支持的会话管理器存储类型：%s)", managerConfig.StoreType)
	}

	return manager
}
