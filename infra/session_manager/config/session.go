package config

import (
	"context"
	"sync"
	"time"

	"gitee.com/hongliu9527/common/infra/base"
	"gitee.com/hongliu9527/common/infra/common"
	"github.com/pkg/errors"
)

// SessionStoreType 会话存储类型
type SessionStoreType string

// 常量相关定义
const (
	SessionModuleName          = "Session"            // 配置模块名
	SessionInfraConfigFileName = "infra.session.yaml" // 会话基础设施配置文件名称

	MemorySession SessionStoreType = "memory" // 内存会话
	RedisSession  SessionStoreType = "redis"  // redis会话
)

var (
	// once 只执行一次
	once sync.Once

	// singleton 会话管理器基础设施配置单例对象
	singleton SessionManagerInfraConfig
)

// SessionManagerInfraConfig 会话管理器基础设施配置
type SessionManagerInfraConfig struct {
	StoreType     SessionStoreType `yaml:"storeType" default:"memory"` // 会话存储类型
	MaxOnline     int              `yaml:"maxOnline" default:"50000"`  // 最大在线用户数
	WebExpireTime int              `yaml:"webExpireTime" default:"2"`  // 网页登录过期时间 单位(小时)
	AppExpireTime int              `yaml:"appExpireTime" default:"48"` // APP登录过期时间 单位(小时)

	base.BaseConfig `yaml:"-"` // 基础配置信息
}

// New 创建Session基础设施配置
func New(source common.ConfigSource) (*SessionManagerInfraConfig, error) {
	singleton.BaseConfig = base.NewBaseConfig(SessionModuleName, SessionInfraConfigFileName)
	err := source.Read(SessionInfraConfigFileName, &singleton, 20*time.Second)
	if err != nil {
		return nil, errors.WithMessage(err, "读取Session基础设施配置信息失败")
	}

	once.Do(func() {
		go singleton.ListenSource(context.TODO(), source, &singleton)
	})

	return &singleton, nil
}
