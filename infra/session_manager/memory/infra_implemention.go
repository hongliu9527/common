package memory

import (
	"context"
	"time"

	"gitee.com/hongliu9527/common/infra/common"

	cmap "github.com/orcaman/concurrent-map/v2"
)

// 常量相关定义
const (
	MemorySessionManager string = "MemorySessionManager" // 内存会话管理器
)

// Name 获取基础设施名
func (m *memorySessionManager) Name() string {
	return MemorySessionManager
}

// start 开启基础设施
func (m *memorySessionManager) start(ctx context.Context) error {
	m.ctx, m.cancel = context.WithCancel(ctx)
	m.UserDatas = cmap.New[common.UserData]()

	// 开启定时清理过期会话协程
	go func() {
		ticker := time.NewTicker(time.Duration(m.config.WebExpireTime/2) * time.Hour)
		for {
			select {
			case <-m.ctx.Done(): // 基础设施关闭
				ticker.Stop()
				return
			case <-ticker.C: // 检查在线用户
				m.checkUserDatas()
			}
		}
	}()

	return nil
}

// checkUserDatas 检查在线用户
func (m *memorySessionManager) checkUserDatas() {
	UserDataiter := m.UserDatas.IterBuffered()
	logoutUserIDs := make([]string, 0)
	for userTouple := range UserDataiter {
		userId := userTouple.Key
		UserData := userTouple.Val

		sessions := UserData.GetSessions()
		for _, session := range sessions {
			switch session.LoginType {
			case common.Web: // 清除过期的网页登录用户
				if session.LastOperateTime == nil || time.Since(*session.LastOperateTime) > time.Duration(m.config.WebExpireTime)*time.Hour {
					UserData.RemoveSession(session.ID)
				}
			case common.App: // 清除过期的APP登录用户
				if session.LastOperateTime == nil || time.Since(*session.LastOperateTime) > time.Duration(m.config.AppExpireTime)*time.Hour {
					UserData.RemoveSession(session.ID)
				}
			}
		}

		// 如果没有会话，则加入到待清理用户ID列表
		if len(UserData.GetSessions()) == 0 {
			logoutUserIDs = append(logoutUserIDs, userId)
		}

		// 清除完全过期用户
		for _, logoutUserID := range logoutUserIDs {
			m.UserDatas.Remove(logoutUserID)
		}
	}
}

// stop 关闭基础设施
func (m *memorySessionManager) stop() error {
	m.cancel()
	return nil
}
