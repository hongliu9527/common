package memory

import (
	"context"

	"gitee.com/hongliu9527/common/infra/base"
	"gitee.com/hongliu9527/common/infra/common"
	"gitee.com/hongliu9527/common/infra/session_manager/config"

	cmap "github.com/orcaman/concurrent-map/v2"
)

// 内存会话管理器单例
var singleton memorySessionManager

// memorySessionManager 内存会话管理器
type memorySessionManager struct {
	base.BaseInfra                                             // 基础设施基类
	config         *config.SessionManagerInfraConfig           // 会话管理器配置信息
	UserDatas      cmap.ConcurrentMap[string, common.UserData] // 全部在线用互信息
	ctx            context.Context                             // 上下文
	cancel         context.CancelFunc                          // 上线文退出函数
}

// New 创建内存会话管理器基础设施
func New(config *config.SessionManagerInfraConfig) common.SessionManagerInfra {
	singleton.config = config

	// 构建基础设施基类
	singleton.BaseInfra = base.NewBaseInfra(singleton.Name(), config, singleton.start, singleton.stop)

	return &singleton
}
