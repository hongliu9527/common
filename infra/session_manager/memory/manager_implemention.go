package memory

import (
	"fmt"
	"time"

	"gitee.com/hongliu9527/common/infra/common"
	"gitee.com/hongliu9527/go-tools/logger"
)

// HasUser 是否存在
func (m *memorySessionManager) HasUser(userID string) bool {
	return m.UserDatas.Has(userID)
}

// GetUserSession 获取用户会话
func (m *memorySessionManager) GetUserSession(userID, sessionID string) (*common.Session, error) {
	if m.UserDatas.Has(userID) {
		if user, ok := m.UserDatas.Get(userID); ok {
			session, err := user.GetSession(sessionID)
			if err != nil {
				return nil, fmt.Errorf("用户(%s)的会话(%s)不存在", userID, sessionID)
			} else {
				return session, nil
			}
		} else {
			return nil, fmt.Errorf("用户(%s)的会话(%s)不存在", userID, sessionID)
		}
	}
	return nil, fmt.Errorf("用户(%s)不存在", userID)
}

// HasUserSession 用户会话是否存在
func (m *memorySessionManager) HasUserSession(userID, sessionID string) bool {
	if m.UserDatas.Has(userID) {
		if user, ok := m.UserDatas.Get(userID); ok {
			_, err := user.GetSession(sessionID)
			if err == nil {
				return true
			}
		}
	}
	return false
}

// AddUserData 新增在线用户
func (m *memorySessionManager) AddUserData(userData interface{}) error {
	user, ok := userData.(common.UserData)
	if !ok {
		logger.Error("(%v)用户没有实现UserData接口", userData)
		return fmt.Errorf("(%v)用户没有实现UserData接口", userData)
	}

	// 检查用户ID
	userID := user.GetUserID()
	if userID == "" {
		logger.Error("(%v)用户ID为空", userData)
		return fmt.Errorf("(%v)用户ID为空", userData)
	}

	// 判断是否达到最大用户数
	if m.config.MaxOnline < m.UserDatas.Count()+1 {
		logger.Error("达到最大用户数(%d)", m.config.MaxOnline)
		return fmt.Errorf("达到最大用户数(%d)", m.config.MaxOnline)
	}

	// 新增用户
	m.UserDatas.Set(userID, user)
	return nil
}

// AddUserSesison 新增在线用户会话
func (m *memorySessionManager) AddUserSession(userID string, session *common.Session) error {
	user, ok := m.UserDatas.Get(userID)
	if !ok {
		logger.Error("(%s)用户在线信息不存在", userID)
		return fmt.Errorf("(%s)用户在线信息不存在", userID)
	}

	return user.UpdateSession(session)
}

// RemoveUserData 删除一个在线用户
func (m *memorySessionManager) RemoveUserData(userID string) {
	m.UserDatas.Remove(userID)
}

// RemoveUserSession 删除用户会话
func (m *memorySessionManager) RemoveUserSession(userID, sessionID string) {
	user, ok := m.UserDatas.Get(userID)
	if !ok {
		return
	}

	user.RemoveSession(sessionID)
}

// UpdateUserSessionTime 更新用户会话时间
func (m *memorySessionManager) UpdateUserSessionTime(userID, sessionID string, time *time.Time) error {
	user, ok := m.UserDatas.Get(userID)
	if !ok {
		return fmt.Errorf("(%s)用户在线信息不存在", userID)
	}

	session, err := user.GetSession(sessionID)
	if err != nil {
		return fmt.Errorf("(%s)用户的在线信息(%s)不存在", userID, sessionID)
	}

	session.LastOperateTime = time
	return nil
}

// Clear 清空全部在线信息
func (m *memorySessionManager) Clear() {
	m.UserDatas.Clear()
}
