/*
 * @Author: hongliu
 * @Date: 2022-09-21 10:37:45
 * @LastEditors: hongliu
 * @LastEditTime: 2023-01-16 16:04:39
 * @FilePath: \common\infra\config_source\local\local.go
 * @Description:Local配置数据源定义
 *
 * Copyright (c) 2022 by 洪流, All Rights Reserved.
 */

package local

import (
	"context"
	"fmt"
	"os"
	"strings"
	"sync"
	"time"

	"gitee.com/hongliu9527/common/infra/common"

	"gitee.com/hongliu9527/go-tools/logger"
	"github.com/spf13/viper"
)

// LOCAL 在系统中的标识
const LOCAL = "local"

// LocalConfigSource 数据源配置定义
type LocalConfigSource struct {
	moduleName                  string            // 模块名称
	basePath                    string            // 文件路径
	lastFileChangeTimestampInfo map[string]uint32 // 文件上次修改时间信息 文件名 - 上次修改时间
	rwlock                      sync.RWMutex      // 文件过期信息读写锁

	ctx    context.Context    // 上下文对象
	cancel context.CancelFunc // 退出回调函数
}

// New 新建本地数据源
func New(moduleName, basePath string) *LocalConfigSource {
	return &LocalConfigSource{
		moduleName: moduleName,
		basePath:   basePath,
	}
}

// Init 初始化配置文件数据配置源
func (l *LocalConfigSource) Init(ctx context.Context) error {
	l.ctx, l.cancel = context.WithCancel(ctx)
	l.lastFileChangeTimestampInfo = make(map[string]uint32)

	return nil
}

// Read 读取指定配置文件的配置数据
func (l *LocalConfigSource) Read(filename string, value interface{}, timeout time.Duration) error {
	configFileName := l.basePath + filename

	configFileInfo, err := os.Stat(configFileName)
	if err != nil {
		return fmt.Errorf("获取配置文件信息(%s)的信息失败(%s)", configFileName, err.Error())
	}
	changeTime := uint32(configFileInfo.ModTime().Unix())

	// 如果最新更新时间有变化，则更新时间记录
	if oldChangeTime, ok := l.lastFileChangeTimestampInfo[filename]; ok {
		if oldChangeTime < changeTime {
			l.rwlock.Lock()
			l.lastFileChangeTimestampInfo[filename] = changeTime
			l.rwlock.Unlock()
		}
	} else {
		l.rwlock.Lock()
		l.lastFileChangeTimestampInfo[filename] = changeTime
		l.rwlock.Unlock()
	}

	sepIndex := strings.LastIndex(filename, ".")
	viper.SetConfigType(filename[sepIndex+1:])
	viper.SetConfigName(filename[:sepIndex])
	viper.AddConfigPath(l.basePath)

	if err := viper.ReadInConfig(); err != nil {
		return fmt.Errorf("读取模块(%s)的配置文件(%s)失败(%s)", l.moduleName, configFileName, err.Error())
	}

	// 反序列化配置信息
	configData := viper.AllSettings()
	err = common.DecodeConfig(configData, value)
	if err != nil {
		return fmt.Errorf("反序列化配置信息失败(%s)", err.Error())
	}

	return nil
}

// Listen 读取指定配置文件的配置数据事件，通过对比文件文件修改时间戳，判断文件是否被改变
func (l *LocalConfigSource) Listen(filename string, value interface{}, timeout time.Duration) error {
	timeoutCtx, timeoutCancel := context.WithTimeout(l.ctx, timeout)
	defer timeoutCancel()

	configFileName := l.basePath + filename
	fileInfo, err := os.Stat(configFileName)
	if err != nil {
		return fmt.Errorf("获取模块(%s)的配置文件(%s)的信息失败(%s),请及时处理", l.moduleName, configFileName, err.Error())
	}
	currentModTimestamp := uint32(fileInfo.ModTime().Unix())

	l.rwlock.RLock()
	lastChangeTime, ok := l.lastFileChangeTimestampInfo[filename]
	l.rwlock.RUnlock()

	if ok && currentModTimestamp > lastChangeTime {
		logger.Debug("监听到(%s)配置数据发生变化", filename)
		return l.Read(filename, value, 20*time.Second)
	}

	select {
	case <-l.ctx.Done():
		return common.ErrAdvanceExit
	case <-timeoutCtx.Done():
		return common.ErrReceiveEventTimeout
	}
}
