/*
 * @Author: hongliu
 * @Date: 2022-12-20 16:43:11
 * @LastEditors: hongliu
 * @LastEditTime: 2022-12-26 11:35:56
 * @FilePath: \common\infra\common\event_bus.go
 * @Description: 事件总线接口定义
 *
 * Copyright (c) 2022 by 洪流, All Rights Reserved.
 */

package common

import "time"

// EventBus 事件总线接口定义
type EventBus interface {
	Subscribe(topic string, subscriber string, fn interface{}) error                                // 订阅主题，"subscriber"参数会表明本次订阅者的身份
	Unsubscribe(topic string, subscriber string) error                                              // 取消已经订阅的单个主题，需要表明订阅者身份
	Publish(topic string, args ...interface{}) error                                                // 发布事件到总线
	PublishWithReply(topic string, timeout time.Duration, args ...interface{}) (interface{}, error) // 目前只支持返回一个结果和err信息函数的handler
}
