package common

import "time"

// LoginType 登录类型
type LoginType uint8

const (
	Web LoginType = 1 // 网页登录
	App LoginType = 2 // APP登录
)

// SessionManager 会话管理器
type SessionManager interface {
	HasUser(userID string) bool                                          // 用户是否存在
	HasUserSession(userID, sessionID string) bool                        // 用户会话是否存在
	GetUserSession(userID, sessionID string) (*Session, error)           // 获取用户会话
	AddUserData(userData interface{}) error                              // 新增在线用户
	AddUserSession(userID string, session *Session) error                // 新增在线用户会话
	RemoveUserData(userID string)                                        // 删除一个在线用户
	RemoveUserSession(userID, sessionID string)                          // 移除在线用户会话
	UpdateUserSessionTime(userID, session string, time *time.Time) error // 更新在线用户
	Clear()                                                              // 清空全部用户
}

// UserData 用户信息
type UserData interface {
	GetUserID() string                             // 获取用户ID
	GetSession(sessionID string) (*Session, error) // 获取会话
	GetSessions() []*Session                       // 获取全部会话
	UpdateSession(session *Session) error          // 更新会话，如果不存在则新增
	RemoveSession(sessionID string)                // 移除会话
}

// Session 会话信息
type Session struct {
	ID              string     // 会话ID
	UserID          uint       // 用户ID
	RoleIDs         []uint     // 角色ID列表
	AccountID       uint       // 账号ID
	LoginType       LoginType  // 登录类型
	IP              string     // 登录IP地址
	LastOperateTime *time.Time // 最近操作时间
}
