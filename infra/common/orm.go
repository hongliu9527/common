/*
 * @Author: hongliu
 * @Date: 2022-09-21 15:58:54
 * @LastEditors: hongliu
 * @LastEditTime: 2023-07-24 14:32:13
 * @FilePath: \common\infra\common\orm.go
 * @Description:Orm基础设施接口定义
 *
 * Copyright (c) 2022 by 洪流, All Rights Reserved.
 */
package common

import (
	"errors"
	"fmt"
	"strings"
)

// Orm Orm基础设施接口定义
type Orm interface {
	DB(databaseName ...string) (orm Orm, err error)                    // 通过库名称获取数据库连接
	Table(table string) (orm Orm, err error)                           // 切换数据表
	Get(dest interface{}, query string, args ...interface{}) error     // 查询单个数据
	Select(dest interface{}, query string, args ...interface{}) error  // 查询多个数据
	Insert(value interface{}) (uint64, error)                          // 插入单个数据
	BatchInsert(values interface{}) error                              // 批量插入
	Update(condition string, updateValue map[string]interface{}) error // 更新数据
	Delete(condition string, args ...interface{}) error                // 删除数据
	Exec(query string, args ...interface{}) error                      // 执行原生sql
	Begin() error                                                      // 开启事务
	Rollback() error                                                   // 回滚事务
	Commit() error                                                     // 执行事务
}

// OrmInfraInstance Orm基础设施纯接口实例
type OrmInfraInstance struct {
	Orm
	Infra
}

// FilterParam 筛选参数
type FilterParam struct {
	Name     string   // 筛选参数名称
	Values   []string // 筛选参数值列表
	Operator string   // 筛选条件操作
}

// SortParm 排序参数
type SortParm struct {
	Name      string // 排序条件名称
	Direction string // 排序方向
}

// BuildQuerySqlByParams 根据参数创建查询语句和参数列表
func BuildQuerySqlByParams(tableName string, filters []FilterParam, sorts []SortParm) (string, []interface{}, error) {
	if tableName == "" {
		return "", nil, errors.New("表名不能为空")
	}

	sql := fmt.Sprintf("SELECT * FROM %s WHERE ", tableName) // 查询语句
	args := make([]interface{}, 0)                           // 参数列表

	// 处理过滤语句数组
	if len(filters) > 0 {
		filterConditions := make([]string, 0, len(filters))
		for _, filter := range filters {
			// 如果是in查询
			if filter.Operator == "in" && len(filter.Values) > 0 {

				filterConditions = append(filterConditions, fmt.Sprintf(" %s IN (?) ", filter.Name))
				args = append(args, filter.Values)
				continue
			}

			// 如果是like查询
			if filter.Operator == "like" && len(filter.Values) == 1 {
				filterConditions = append(filterConditions, fmt.Sprintf(" %s LIKE ? ", filter.Name))
				args = append(args, "%"+filter.Values[0]+"%")
				continue
			}

			// 如果操作符为空
			if filter.Operator == "" && len(filter.Values) == 1 {
				filterConditions = append(filterConditions, fmt.Sprintf(" %s = ? ", filter.Name))
				args = append(args, "%"+filter.Values[0]+"%")
				continue
			}

			// 其他查询
			if len(filter.Values) == 1 {
				filterConditions = append(filterConditions, fmt.Sprintf(" %s %s ? ", filter.Name, filter.Operator))
				args = append(args, filter.Values[0])
			}
		}
		if len(filterConditions) > 0 {
			sql = fmt.Sprintf("%s %s", sql, strings.Join(filterConditions, "AND"))
		} else {
			sql += " is_delete = 0"
		}
	} else {
		sql += " is_delete = 0"
	}

	// 处理排序参数数组
	sortConditions := make([]string, 0, len(sorts))
	if len(sorts) > 0 {
		for _, sort := range sorts {
			sortConditions = append(sortConditions, fmt.Sprintf(" %s %s ", sort.Name, sort.Direction))
		}
	}
	if len(sortConditions) > 0 {
		sql += fmt.Sprintf(" ORDER BY %s ", strings.Join(sortConditions, ","))
	}

	return sql, args, nil
}

// BuildQueryPageSqlByParams 通过参数创建分页查询语句和参数列表
func BuildQueryPageSqlByParams(tableName string, filters []FilterParam, sorts []SortParm, currentPage, pageSize uint) (string, []interface{}, error) {
	sql, args, err := BuildQuerySqlByParams(tableName, filters, sorts)
	if err != nil {
		return "", nil, err
	}

	// 处理分页参数
	sql += fmt.Sprintf(" LIMIT %d,%d", pageSize*(currentPage-1), pageSize)

	return sql, args, nil
}

// BuildCountSqlByParams 根据参数创建统计语句和参数列表
func BuildCountSqlByParams(tableName string, filters []FilterParam) (string, []interface{}, error) {
	if tableName == "" {
		return "", nil, errors.New("表名不能为空")
	}

	sql := fmt.Sprintf("SELECT COUNT(*) FROM %s WHERE ", tableName) // 查询语句
	args := make([]interface{}, 0)                                  // 参数列表

	// 处理过滤语句数组
	filterConditions := make([]string, 0, len(filters))
	if len(filters) > 0 {
		for _, filter := range filters {
			// 如果是in查询
			if filter.Operator == "in" && len(filter.Values) > 0 {
				filterConditions = append(filterConditions, fmt.Sprintf(" %s IN (?) ", filter.Name))
				args = append(args, filter.Values)
				continue
			}

			// 如果是like查询
			if filter.Operator == "like" && len(filter.Values) == 1 {
				filterConditions = append(filterConditions, fmt.Sprintf(" %s LIKE ? ", filter.Name))
				args = append(args, "%"+filter.Values[0]+"%")
				continue
			}

			// 如果操作符为空
			if filter.Operator == "" && len(filter.Values) == 1 {
				filterConditions = append(filterConditions, fmt.Sprintf(" %s = ? ", filter.Name))
				args = append(args, "%"+filter.Values[0]+"%")
				continue
			}

			// 其他查询
			if len(filter.Values) == 1 {
				filterConditions = append(filterConditions, fmt.Sprintf(" %s %s ? ", filter.Name, filter.Operator))
				args = append(args, filter.Values[0])
			}
		}
		if len(filterConditions) > 0 {
			sql = fmt.Sprintf("%s %s", sql, strings.Join(filterConditions, "AND"))
		} else {
			sql += " is_delete = 0"
		}
	} else {
		sql += " is_delete = 0"
	}

	return sql, args, nil
}

// BuildConditionByFilters 根据过滤参数创建条件
func BuildConditionByFilters(filters []FilterParam) string {
	// 处理过滤语句数组
	condition := ""
	filterConditions := make([]string, 0, len(filters))
	if len(filters) > 0 {
		for _, filter := range filters {
			// 如果是in查询
			if filter.Operator == "in" && len(filter.Values) > 0 {
				filterConditions = append(filterConditions, fmt.Sprintf(" %s IN (%s) ", filter.Name, strings.Join(filter.Values, ",")))
				continue
			}

			// 如果是like查询
			if filter.Operator == "like" && len(filter.Values) == 1 {
				filterConditions = append(filterConditions, fmt.Sprintf(" %s LIKE '%%%s%%' ", filter.Name, filter.Values[0]))
				continue
			}

			// 如果操作符为空
			if filter.Operator == "" && len(filter.Values) == 1 {
				filterConditions = append(filterConditions, fmt.Sprintf(" %s = '%%%s%%' ", filter.Name, filter.Values[0]))
				continue
			}

			// 其他查询
			if len(filter.Values[0]) == 1 {
				filterConditions = append(filterConditions, fmt.Sprintf(" %s %s %s ", filter.Name, filter.Operator, filter.Values[0]))
			}
		}
		if len(filterConditions) > 0 {
			condition = strings.Join(filterConditions, "AND")
		}
	}

	return condition
}
