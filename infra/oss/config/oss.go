/*
 * @Author: hongliu
 * @Date: 2022-09-24 16:26:04
 * @LastEditors: hongliu
 * @LastEditTime: 2022-12-26 10:48:28
 * @FilePath: \common\infra\oss\config\oss.go
 * @Description: oss 配置信息
 *
 * Copyright (c) 2022 by 洪流, All Rights Reserved.
 */

package config

import (
	"context"
	"sync"
	"time"

	"gitee.com/hongliu9527/common/infra/base"
	"gitee.com/hongliu9527/common/infra/common"

	"gitee.com/hongliu9527/go-tools/uuid"
	"github.com/pkg/errors"
)

// OssServiceVendor Oss运营商类型定义
type OssServiceVendor string

const (
	// Oss运营商相关定义
	AliYun      OssServiceVendor = "aliyun"      // 阿里云
	HuaWeiCloud OssServiceVendor = "huaweicloud" // 华为云
	Ctyun       OssServiceVendor = "ctyun"       // 天翼云
	Local       OssServiceVendor = "local"       // 本地oss

	// files oos 目录结构
	Root            = "files"          // oss桶下的一级目录
	SystemDirectory = Root + "/system" // 平台下的二级目录, 存放系统资源文件
	CommonDirectory = Root + "/common" // 平台下的二级目录, 存放普通文件

	// 配置文件相关
	OssModuleName          = "Oss"            // 配置模块名
	OssInfraConfigFileName = "infra.oss.yaml" // Oss基础设施配置文件名称
)

var (
	// singleton Oss基础设施配置单例对象
	singleton OssInfraConfig

	// 只执行一次
	once sync.Once
)

// SystemFileDirectory 获取系统资源文件对应的oss目录
func SystemFileDirectory() string {
	return SystemDirectory + "/" + uuid.UUID() + "/"
}

// CommonFileDirectory 获取媒体文件对应的oss目录
func CommonFileDirectory() string {
	return CommonDirectory + "/" + time.Now().Format("20060102") + "/"
}

// OssInfraConfig Oss基础设施配置结构定义
type OssInfraConfig struct {
	ServiceVendor OssServiceVendor `yaml:"serviceVendor" default:"aliyun"` // Oss运营商
	Compress      bool             `yaml:"compress" default:"true"`        // 上传文件是否压缩
	RetryCount    int              `yaml:"retryCount" default:"3"`         // 上传失败重试次数
	// Oss访问地址和密钥相关配置
	AccessKeyID          string     `yaml:"accessKeyID" default:"ID"`             // 数据访问KEY标识
	AccessKeySecret      string     `yaml:"accessKeySecret" default:"keySecret"`  // 数据访问密钥
	Endpoint             string     `yaml:"endpoint" default:"127.0.0.1"`         // 数据挂载点名称
	Bucket               string     `yaml:"bucket" default:"bucket-02"`           // 数据仓库名称
	RoleARN              string     `yaml:"roleARN" default:"testRole"`           // 临时角色访问ARN
	SignatureExpiresTime int        `yaml:"signatureExpiresTime" default:"10800"` // 签名过期时间
	base.BaseConfig      `yaml:"-"` // 基础配置信息
}

// New 创建Oss基础设施配置
func New(source common.ConfigSource) (*OssInfraConfig, error) {
	singleton.BaseConfig = base.NewBaseConfig(OssModuleName, OssInfraConfigFileName)
	err := source.Read(OssInfraConfigFileName, &singleton, 20*time.Second)
	if err != nil {
		return nil, errors.WithMessage(err, "读取Oss基础设施配置信息失败")
	}

	once.Do(func() {
		go singleton.ListenSource(context.TODO(), source, &singleton)
	})

	return &singleton, nil
}
