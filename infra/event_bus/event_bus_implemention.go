/*
 * @Author: hongliu
 * @Date: 2022-12-20 17:05:37
 * @LastEditors: hongliu
 * @LastEditTime: 2022-12-20 17:24:18
 * @FilePath: \common\infra\event_bus\event_bus_implemention.go
 * @Description: 事件总线接口实现
 *
 * Copyright (c) 2022 by 洪流, All Rights Reserved.
 */

package event_bus

import (
	"fmt"
	"time"

	"errors"
)

// Subscribe 订阅主题，"subscriber"参数会表明本次订阅操作者的身份
func (i *eventBusInfra) Subscribe(topic string, subscriber string, fn interface{}) error {
	if i.bus == nil {
		return errors.New("事件总线基础设施未启动，无法订阅事件")
	}

	err := i.bus.Subscribe(topic, fn)
	if err != nil {
		return fmt.Errorf("订阅失败(%s)", err.Error())
	}

	return i.addHandler(topic, subscriber, fn)
}

// addHandler 增加处理句柄记录
// 支持相同的topic、subscriber和fn的重复订阅.取消订阅的顺序和订阅的顺序一致.
func (i *eventBusInfra) addHandler(topic string, subscriber string, fn interface{}) error {
	i.lock.Lock()
	defer i.lock.Unlock()

	if i.subscribedHandlers == nil {
		return errors.New("事件总线句柄集合为空,请检查事件总线基础设施是否为启动状态")
	}

	handlers, ok := i.subscribedHandlers[topic]
	if !ok {
		i.subscribedHandlers[topic] = []handler{}
		handlers = []handler{}
	}

	handlers = append(handlers, handler{
		callback:   fn,
		subscriber: subscriber,
	})

	i.subscribedHandlers[topic] = handlers

	return nil
}

// Unsubscribe 取消已经订阅的单个主题，需要表明订阅者身份
func (i *eventBusInfra) Unsubscribe(topic string, subscriber string) error {
	if i.bus == nil {
		return errors.New("事件总线基础设施未启动，无法取消订阅事件")
	}

	fn, err := i.queryHandler(topic, subscriber)
	if err != nil {
		return err
	}

	return i.bus.Unsubscribe(topic, fn)
}

// queryHandler 查询处理句柄记录
func (i *eventBusInfra) queryHandler(topic string, subscriber string) (interface{}, error) {
	i.lock.RLock()
	defer i.lock.RUnlock()

	if i.subscribedHandlers == nil {
		return nil, errors.New("事件总线句柄集合为空,请检查事件总线基础设施是否为启动状态")
	}

	handlers, ok := i.subscribedHandlers[topic]
	if !ok {
		return nil, fmt.Errorf("查询不到Topic(%s)相关的订阅记录", topic)
	}

	for _, handler := range handlers {
		if handler.subscriber == subscriber {
			return handler.callback, nil
		}
	}

	return nil, fmt.Errorf("查询不到订阅者(%s)订阅的Topic(%s)相关记录", subscriber, topic)
}

// Publish 发布事件到总线，广播
func (i *eventBusInfra) Publish(topic string, args ...interface{}) error {
	if i.bus == nil {
		return errors.New("事件总线基础设施未启动，无法发布事件")
	}

	i.bus.Publish(topic, args...)

	return nil
}

// PublishWithReply 发布事件到总线，单个调用，并带超时控制和返回结果
func (i *eventBusInfra) PublishWithReply(topic string, timeout time.Duration, args ...interface{}) (interface{}, error) {
	if i.bus == nil {
		return nil, errors.New("事件总线基础设施未启动，无法发布事件")
	}

	return i.bus.PublishWithReply(topic, timeout, args...)
}
