/*
 * @Author: hongliu
 * @Date: 2022-12-20 17:02:48
 * @LastEditors: hongliu
 * @LastEditTime: 2022-12-20 17:04:25
 * @FilePath: \common\infra\event_bus\infra_implementation.go
 * @Description: 基础设施接口实现
 *
 * Copyright (c) 2022 by 洪流, All Rights Reserved.
 */

package event_bus

import (
	"context"

	"gitee.com/hongliu9527/go-tools/eventbus"
)

// 常量相关定义
const (
	infraName = "EventBus" // 基础设施(事件总线名)
)

func (i *eventBusInfra) Name() string {
	return infraName
}

// start 事件总线基础设施启动方法
func (i *eventBusInfra) start(ctx context.Context) error {
	i.subscribedHandlers = make(map[string]([]handler))
	i.bus = eventbus.New()

	return nil
}

// stop 事件总线基础设施停止方法
func (i *eventBusInfra) stop() error {
	i.bus = nil
	i.subscribedHandlers = nil

	return nil
}
