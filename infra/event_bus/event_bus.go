/*
 * @Author: hongliu
 * @Date: 2022-12-20 16:36:50
 * @LastEditors: hongliu
 * @LastEditTime: 2022-12-20 17:02:32
 * @FilePath: \common\infra\event_bus\event_bus.go
 * @Description:事件总线基础设施
 *
 * Copyright (c) 2022 by 洪流, All Rights Reserved.
 */

package event_bus

import (
	"sync"

	"gitee.com/hongliu9527/common/infra/base"
	"gitee.com/hongliu9527/common/infra/common"
	"gitee.com/hongliu9527/go-tools/eventbus"
)

// handler 处理句柄结构体定义
type handler struct {
	callback   interface{} // 句柄触发对应的执行回调函数
	subscriber string      // 记录该句柄是由哪个订阅者订阅得到的
}

// eventBusInfra 事件总线基础设施
type eventBusInfra struct {
	base.BaseInfra                            // 基础设施基类
	bus                eventbus.Bus           // 事件总线实例对象
	subscribedHandlers map[string]([]handler) // 已成功订阅的处理句柄的记录集合(包含订阅者身份)
	lock               sync.RWMutex           // 读写锁,用于处理并发订阅发布
}

// singleton EventBus基础设施单例对象
var singleton eventBusInfra

// New 返回事件总线基础设施单例对象
func New() common.EventBusInfra {
	singleton.BaseInfra = base.NewBaseInfra(singleton.Name(), nil, singleton.start, singleton.stop)

	return &singleton
}
